import controller.DrawingLoop;
import controller.GameLoop;
import de.saxsys.mvvmfx.testingutils.jfxrunner.JfxRunner;
import javafx.scene.input.KeyCode;
import model.Character;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import view.Platform;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import static org.junit.Assert.*;


public class CharacterTest {
    private Character floatingCharacter;
    private Character floatingCharacter2;
    private ArrayList<Character> characterListUnderTest;
    private GameLoop gameLoopUnderTest;
    private DrawingLoop drawingLoopUnderTest;



    @Before
    public void setup() {
        floatingCharacter = new Character(28, 30,0,270, KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.S);
        floatingCharacter2 = new Character(Platform.WIDTH-60, 30,0,270, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.DOWN);
        characterListUnderTest = new ArrayList<Character>();
        characterListUnderTest.add(floatingCharacter);
        characterListUnderTest.add(floatingCharacter2);
    }

    @Test
    public void characterInitialValuesShouldMatchConstructorArguments() {
        assertEquals("Initialx", 28, floatingCharacter.getX(), 0);
        assertEquals("Initialy", 30, floatingCharacter.getY(), 0);
        assertEquals("Offsetx", 0, floatingCharacter.getOffsetX(), 0.0);
        assertEquals("Offsety", 270, floatingCharacter.getOffsetY(), 0.0);
        assertEquals("Leftkey", KeyCode.A, floatingCharacter.getLeftKey());
        assertEquals("Rightkey", KeyCode.D, floatingCharacter.getRightKey());
        assertEquals("Upkey", KeyCode.W, floatingCharacter.getUpKey());
        assertEquals("Upkey", KeyCode.S, floatingCharacter.getDownKey());
        assertEquals("Initialx", Platform.WIDTH-60, floatingCharacter2.getX(), 0);
        assertEquals("Initialy", 30, floatingCharacter2.getY(), 0);
        assertEquals("Offsetx", 0, floatingCharacter2.getOffsetX(), 0.0);
        assertEquals("Offsety", 270, floatingCharacter2.getOffsetY(), 0.0);
        assertEquals("Leftkey", KeyCode.LEFT, floatingCharacter2.getLeftKey());
        assertEquals("Rightkey", KeyCode.RIGHT, floatingCharacter2.getRightKey());
        assertEquals("Upkey", KeyCode.UP, floatingCharacter2.getUpKey());
        assertEquals("Upkey", KeyCode.DOWN, floatingCharacter2.getDownKey());
    }

    @Test
    public void CharacterSpawnAtSpawnPoint() throws IllegalAccessException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        assertTrue("View: Character is spawn", characterUnderTest.getX() == startX);
    }

    @Test
    public void CharacterIsMoveRightWhenPressRight() throws IllegalAccessException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        assertTrue("View: Character is spawn", characterUnderTest.getX() == startX);
    }

    @Test
    public void CharacterIsMoveRightWhenPressLeft() throws IllegalAccessException, NoSuchFieldException {
        Character characterUnderTest = characterListUnderTest.get(0);
        int startX = characterUnderTest.getX();
        assertTrue("View: Character is spawn", characterUnderTest.getX() == startX);
    }




}
