package controller;

import javafx.scene.input.KeyCode;
import model.Character;
import view.Platform;
import model.Ball;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void checkDrawCollisions(ArrayList<Character> characterList, Ball ball) throws InterruptedException {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            character.checkReachPillar();
        }
        ball.checkReachGameWall();
        ball.checkReachFloor();
        ball.checkReachHighest();
        ball.checkReachNet();
        for(Character cA:characterList) {
            for(Character cB:characterList) {
                if(cA!=cB) {
                    if(cA.getBoundsInParent().intersects(ball.getBoundsInParent())) {
                        ball.normalHit(2);
                    }else if(cB.getBoundsInParent().intersects(ball.getBoundsInParent())){
                        ball.normalHit(1);
                    }
                    else if(ball.reachedFloor()){
                        if(ball.getX()>400){
                            if(cA.getGauge() >= 5){
                            cA.setScore(3);
                            }else cA.setScore(1);
                            cA.resetGauge();
                            cB.increaseGauge();
                            platform.getKeys().remove(KeyCode.L);
                            platform.getKeys().remove(KeyCode.B);
                            cA.larger();
                            cB.larger();
                            /*TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();*/
                            ball.setFirstserve(1);
                            if(Math.abs(cA.getScore()-cB.getScore()) == 5){
                                ball.slowdown();
                            }else if(Math.abs(cA.getScore()-cB.getScore()) == 10){
                                ball.speedup();
                            }else ball.normalize();
                            ball.respawn();
                        }
                        else if(ball.getX()<400){
                            if(cB.getGauge() >= 5){
                            cB.setScore(3);
                            }else cB.setScore(1);
                            cB.resetGauge();
                            cA.increaseGauge();
                            platform.getKeys().remove(KeyCode.L);
                            platform.getKeys().remove(KeyCode.B);
                            cA.larger();
                            cB.larger();
                            /*TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();*/
                            ball.setFirstserve(2);
                            if(Math.abs(cA.getScore()-cB.getScore()) == 5){
                                ball.slowdown();
                            }else if(Math.abs(cA.getScore()-cB.getScore()) == 10){
                                ball.speedup();
                            }else ball.normalize();
                            ball.respawn();
                        }
                    }
                }
            }
        }
    }
    private void checkDrawCollisions1(ArrayList<Character> characterList, Ball ball1) throws InterruptedException {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
            character.checkReachPillar();
        }
        ball1.checkReachGameWall();
        ball1.checkReachFloor();
        ball1.checkReachHighest();
        ball1.checkReachNet();
        for(Character cA:characterList) {
            for(Character cB:characterList) {
                if(cA!=cB) {
                    if(cA.getBoundsInParent().intersects(ball1.getBoundsInParent())) {
                        ball1.normalHit(2);
                    }else if(cB.getBoundsInParent().intersects(ball1.getBoundsInParent())){
                        ball1.normalHit(1);
                    }
                    else if(ball1.reachedFloor()){
                        if(ball1.getX()>400){
                            cA.setScore(1);
                            platform.getKeys().remove(KeyCode.L);
                            platform.getKeys().remove(KeyCode.B);
                            TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();
                            ball1.setFirstserve(1);
                            ball1.respawn();
                        }
                        else if(ball1.getX()<400){
                            cB.setScore(1);
                            TimeUnit.MILLISECONDS.sleep(500);
                            cA.respawn();
                            cB.respawn();
                            ball1.setFirstserve(2);
                            ball1.respawn();
                        }
                    }
                }
            }
        }
    }

    private void paint(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.repaint();
        }
    }

    private void paint(Ball ball){
        ball.repaint();
    }
    private void paint1(Ball ball1){
        ball1.repaint();
    }

    @Override
    public void run() {
        while (running) {
            float time = System.currentTimeMillis();
            try {
                checkDrawCollisions(platform.getCharacterList(), platform.getBall());
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            try {
                checkDrawCollisions(platform.getCharacterList(), platform.getBall1());
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            paint(platform.getCharacterList());
            paint(platform.getBall());
            paint1(platform.getBall1());

            time = System.currentTimeMillis() - time;
            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
