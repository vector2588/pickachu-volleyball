package controller;

import javafx.scene.input.KeyCode;
import model.Ball;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import model.Character;
import view.Platform;
import view.Score;

import java.util.ArrayList;

public class GameLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;

    public GameLoop(Platform platform) {
        this.platform = platform;
        frameRate = 5;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
    }

    private void updateScore(ArrayList<Score>scoreList, ArrayList<Character>characterList) {
        javafx.application.Platform.runLater(() -> {
            for(int i = 0 ; i < scoreList.size() ;i++) {
                scoreList.get(i).setPoint(characterList.get(i).getScore());
                scoreList.get(i).setGauge(characterList.get(i).getGauge());
            }
        });
    }

    private void update(ArrayList<Character> characterList, Ball ball) {

        for (Character character : characterList ) {
            if (platform.getKeys().isPressed(character.getLeftKey()) || platform.getKeys().isPressed(character.getRightKey())) {
                character.getImageView().tick();
            }
            if (platform.getKeys().isPressed(character.getUpKey())) {
                character.getImageView().jump();
                character.setIsOnGround(false);
            }
            if (platform.getKeys().isPressed(character.getDownKey())) {
                character.getImageView().dive();
            }
            if (platform.getKeys().isPressed(character.getDownKey())) {
                character.moveDown();
            }

            if (platform.getKeys().isPressed(character.getLeftKey())) {
                character.setScaleX(-1);
                character.moveLeft();
                character.trace("left");
            }
            if(platform.getKeys().isPressed(KeyCode.R)){
                character.respawn();
                ball.respawn();
                character.resetScore();
            }
            if (platform.getKeys().isPressed(character.getRightKey())) {
                character.setScaleX(1);
                character.moveRight();
                character.trace("right");
            }

            if (!platform.getKeys().isPressed(character.getLeftKey()) && !platform.getKeys().isPressed(character.getRightKey()) ) {
                character.stop();
            }

            if (platform.getKeys().isPressed(character.getUpKey())) {
                character.jump();
            }
        }
    }

    @Override
    public void run() {
        while (running) {
            float time = System.currentTimeMillis();
            update(platform.getCharacterList(),platform.getBall());
            updateScore(platform.getScoreList(),platform.getCharacterList());
            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
