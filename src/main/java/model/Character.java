package model;

import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import view.Platform;

public class Character extends Pane {

    Logger logger = (Logger) LogManager.getLogger(Character.class.getName());

    public int CHARACTER_WIDTH = 65;
    public int CHARACTER_HEIGHT = 65;
    private Image characterImg;
    private int score = 0;
    private AnimatedSprite imageView;
    private int x;
    private int y;
    private KeyCode leftKey;
    private KeyCode rightKey;
    private KeyCode upKey;
    private KeyCode downKey;
    private KeyCode dashKey;
    int xVelocity = 0;
    int yVelocity = 0;
    int xAcceleration = 1;
    int yAcceleration = 1;
    int xMaxVelocity = 7;
    int yMaxVelocity = 17;
    int powerGauge = 0;
    boolean isMoveLeft = false;
    boolean isMoveRight = false;
    boolean falling = true;
    boolean canJump = false;
    boolean jumping = false;
    private boolean isOnGround = true;
    boolean notRespawning = false;
    private int startX;
    private int startY;
    private int offsetX;
    private int offsetY;
    private int countJump = 0;

    public Character(int x, int y, int offsetX, int offsetY, KeyCode leftKey, KeyCode rightKey, KeyCode upKey, KeyCode downKey,KeyCode dashKey) {
        this.startX = x;
        this.startY = y;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.x = x;
        this.y = y;
        this.setTranslateX(x);
        this.setTranslateY(y);
        this.characterImg = new Image(getClass().getResourceAsStream("/assets/AnimeSprite.png"));
        this.imageView = new AnimatedSprite(characterImg,4,4,offsetX,offsetY,65,65);
        this.imageView.setFitWidth(CHARACTER_WIDTH);
        this.imageView.setFitHeight(CHARACTER_HEIGHT);
        this.leftKey = leftKey;
        this.rightKey = rightKey;
        this.upKey = upKey;
        this.downKey = downKey;
        this.dashKey = dashKey;
        this.getChildren().addAll(this.imageView);
    }

    public void collided(Character c) {
        if(isMoveLeft) {
            x = c.getX() + CHARACTER_WIDTH + 1;
            stop();
        }else if(isMoveRight) {
            x = c.getX() - CHARACTER_WIDTH - 1;
            stop();
        }
        if(y<Platform.GROUND-CHARACTER_HEIGHT) {
            if(falling&&y<c.getY()) {
                c.respawn();
            }
        }
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setIsOnGround(boolean isOnGround){
        this.isOnGround = isOnGround;
        System.out.println(isOnGround);
    }

    public void respawn() {
        this.xVelocity = 0;
        this.yVelocity = 0;
        this.xAcceleration = 1;
        this.yAcceleration = 1;
        this.xMaxVelocity = 7;
        this.yMaxVelocity = 17;
        this.powerGauge = 0;
        if(powerGauge<5) {
            this.CHARACTER_HEIGHT = 65;
            this.CHARACTER_WIDTH = 65;
            x=startX;
            y=startY;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }else{
            if(startX<400){
                x=startX;
            }else x=startX-65;
            this.CHARACTER_HEIGHT = 95;
            this.CHARACTER_WIDTH = 95;
            y=startY-100;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }
        isMoveLeft=false;
        isMoveRight=false;
        falling=true;
        canJump=false;
        jumping=false;
        notRespawning=false;
    }

    public void larger(){
        if(powerGauge>5) {
            this.CHARACTER_HEIGHT = 90;
            this.CHARACTER_WIDTH = 90;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }else {
            this.CHARACTER_HEIGHT = 65;
            this.CHARACTER_WIDTH = 65;
            imageView.setFitWidth(CHARACTER_WIDTH);
            imageView.setFitHeight(CHARACTER_HEIGHT);
        }
    }

    public void moveLeft() {
        isMoveLeft = true;
        isMoveRight = false;
    }
    public void moveRight() {
        isMoveRight = true;
        isMoveLeft = false;
    }

    public void moveDown(){
        yVelocity = yVelocity >= 100 ? 100 : xVelocity + 100;
        y = y + yVelocity;
    }

    public void stop() {
        isMoveLeft = false;
        isMoveRight = false;
        xVelocity = 0;
    }

    public void stopPillar(){
        xVelocity = 0;
    }

    public void jump() {
        if (canJump) {
            if (countJump >= 2){
                canJump = false;
            }else{
                yVelocity = yMaxVelocity;
                canJump = true;
                jumping = true;
                falling = false;
                countJump++;
            }
        }
    }

    public void checkReachHighest() {
        if(jumping &&  yVelocity <= 0) {
            jumping = false;
            falling = true;
            yVelocity = 0;
        }
    }

    public void checkReachPillar() {
        if(getX() >= 380-CHARACTER_WIDTH && getX() <= 410-CHARACTER_WIDTH ) {
            isMoveRight = false;
            stopPillar();
        } else if (x >= 400 && x <= 420){
            isMoveLeft = false;
            stopPillar();
        }
    }

    public void checkReachFloor() {
        if(falling && y >= Platform.GROUND - CHARACTER_HEIGHT) {
            falling = false;
            canJump = true;
            yVelocity = 0;
            countJump = 0;
            isOnGround = true;
        }
    }

    public void checkReachGameWall() {
        if(x <= 0) {
            x = 0;
        } else if( x+getWidth() >= Platform.WIDTH) {
            x = Platform.WIDTH-CHARACTER_WIDTH;
        }
    }

    public void moveX() {
        setTranslateX(x);

        if(isMoveLeft && !Platform.getKeys().isPressed(dashKey)) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x - xVelocity;
        }else if(isMoveLeft && Platform.getKeys().isPressed(dashKey)) {
            xVelocity = 10;
            x = x - xVelocity;
        }
        if(isMoveRight && !Platform.getKeys().isPressed(dashKey)) {
            xVelocity = xVelocity >= xMaxVelocity? xMaxVelocity : xVelocity+xAcceleration;
            x = x + xVelocity;
        }else if(isMoveRight && Platform.getKeys().isPressed(dashKey)) {
            xVelocity = 10;
            x = x + xVelocity;
        }
    }

    public void moveY() {
        setTranslateY(y);

        if(falling) {
            yVelocity = yVelocity >= yMaxVelocity? yMaxVelocity : yVelocity+yAcceleration;
            y = y + yVelocity;
        }
        else if(jumping) {
            yVelocity = yVelocity <= 0 ? 0 : yVelocity-yAcceleration;
            y = y - yVelocity;
        }
    }

    public void repaint() {
        moveX();
        moveY();
    }

    public void trace(String log) {
        logger.info("x:{} y:{} vx:{} vy:{} action:{}", x, y, xVelocity, yVelocity, log);

    }

    public KeyCode getLeftKey() {
        return leftKey;
    }

    public KeyCode getRightKey() {
        return rightKey;
    }

    public KeyCode getUpKey() {
        return upKey;
    }

    public KeyCode getDownKey() {
        return downKey;
    }

    public AnimatedSprite getImageView() {
        return imageView;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int scoretoAdd) {
        xMaxVelocity++;
        xVelocity++;
        yMaxVelocity++;
        if (xVelocity > 15){
            this.xVelocity = 0;
            this.xMaxVelocity = 7;
            this.yMaxVelocity = 17;
        }
        this.score = this.score + scoretoAdd;
    }

    public void resetScore(){
        this.score = 0;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }
    public void increaseGauge(){
        powerGauge++;
    }
    public void resetGauge(){
        powerGauge = 0;
    }

    public int getGauge() {
        return powerGauge;
    }
}
