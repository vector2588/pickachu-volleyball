package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import model.Ball;
import model.Character;
import model.Keys;
import javafx.scene.control.Button;

import java.util.ArrayList;

public class Platform extends Pane {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 400;
    public static final int GROUND = 300;

    private Image platformImg;
    private Ball ball;
    private Ball ball1;
    private ArrayList<Character> characterList;
    private ArrayList<Score> scoreList;


    private static Keys keys;

    public Platform() {
        characterList = new ArrayList<>();
        scoreList = new ArrayList();
        keys = new Keys();
        platformImg = new Image(getClass().getResourceAsStream("/assets/Background1.png"));
        ImageView backgroundImg = new ImageView(platformImg);
        backgroundImg.setFitHeight(HEIGHT);
        backgroundImg.setFitWidth(WIDTH);
        characterList.add(new Character(28, 200,0,270, KeyCode.A,KeyCode.D,KeyCode.W,KeyCode.S,KeyCode.V));
        characterList.add(new Character(Platform.WIDTH-100, 200,0,270, KeyCode.LEFT,KeyCode.RIGHT,KeyCode.UP,KeyCode.DOWN,KeyCode.L));
        getChildren().add(backgroundImg);
        ball = new Ball(30, 0);
        ball1=new Ball(60,0);
        getChildren().add(ball);
        getChildren().add(ball1);
        scoreList.add(new Score(30,GROUND+ 30));
        scoreList.add(new Score(Platform.WIDTH-60,GROUND+ 30));
        getChildren().addAll(characterList);
        getChildren().addAll(scoreList);
    }

    public ArrayList<Character> getCharacterList() {
        return characterList;
    }

    public static Keys getKeys() {
        return keys;
    }

    public ArrayList<Score> getScoreList() {
        return scoreList;
    }

    public Ball getBall() {
        return ball;
    }
    public Ball getBall1() {
        return ball1;
    }
}

